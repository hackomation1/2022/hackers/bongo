# 1. Project Title: GONERRRR

< Explain what your project does and why >

## Overview & Features

< features and special functions of your system>

## Demo / Proof of work

< Fotos, video of your working project as proof of work>

## Deliverables

< The deliverables of the project, so we know when it is complete>
- Project poster
- Proof of concept working
- Product pitch deck

## Project landing page/website (optional)

< if you created website or the like for your project link it up here>

## the team & contact

< Time to shine with your team .. show their expertise and contribution to the project >

